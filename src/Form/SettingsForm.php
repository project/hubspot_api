<?php

namespace Drupal\hubspot_api\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use SevenShores\Hubspot\Http\Client;
use SevenShores\Hubspot\Resources\OAuth2;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure HubSpot API settings.
 */
class SettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Construct the SettingsForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(StateInterface $state, ConfigFactoryInterface $config_factory) {
    $this->state = $state;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hubspot_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hubspot_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hubspot_api.settings');

    $form['access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description'   => $this->t('Generate a <a href="https://app.hubspot.com/keys/get" target="_blank">new key</a>.
        Make sure to clear Drupal cache after you change API Key.'),
      '#default_value' => $config->get('access_key'),
    ];

    // Status.
    $title = $this->t('OAuth Connection Status');
    $status = $this->oauthIsConnected() ? $this->t('Connected') : $this->t('Disconnected');
    $color = $this->oauthIsConnected() ? 'green' : 'red';
    $form['status_report'] = [
      '#markup' => '<strong>' . $title . ': </strong><span style="color:' . $color . '">' . $status . '</span>',
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('client_id'),
    ];
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client secret'),
      '#required' => TRUE,
      '#default_value' => $config->get('client_secret'),
    ];
    $form['oauth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('OAuth connection'),
    ];
    $hubspot_api_tokens = $this->state->get('hubspot_api_tokens');
    $form['oauth']['tokens'] = [
      '#type' => 'details',
      '#title' => $this->t('Tokens'),
      '#open' => TRUE,
      '#access' => (bool) $hubspot_api_tokens['access_token'] && $hubspot_api_tokens['refresh_token'],
    ];
    $form['oauth']['tokens']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#required' => FALSE,
      '#default_value' => $hubspot_api_tokens['access_token'],
      '#attributes' => ['readonly' => 'readonly'],
      '#maxlength' => 300,
    ];
    $form['oauth']['tokens']['refresh_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Refresh Token'),
      '#required' => FALSE,
      '#default_value' => $hubspot_api_tokens['refresh_token'],
      '#attributes' => ['readonly' => 'readonly'],
    ];
    $form['oauth']['button'] = [
      '#type' => 'submit',
      '#value' => $this->t('Authorize'),
      '#submit' => ['::submitForm', '::oauthAuthorizeSubmit'],
      '#access' => (bool) !$hubspot_api_tokens['access_token'],
    ];
    $form['oauth']['disconnect'] = [
      '#type' => 'submit',
      '#value' => $this->t('Disconnect'),
      '#limit_validation_errors' => [],
      '#submit' => ['::oauthDisconnectSubmit'],
      '#access' => (bool) $hubspot_api_tokens['access_token'],
    ];

    $form['help'] = [
      '#type' => 'details',
      '#title' => $this->t('Help'),
      '#open' => FALSE,
    ];
    $form['help']['list'] = [
      '#theme' => 'item_list',
      '#type' => 'ol',
      '#items' => [
        $this->t('Log in to <a href="@url" target="_blank">Hubspot</a> developer portal.', ['@url' => 'https://developers.hubspot.com/']),
        $this->t('<a href="@url" target="_blank">Create an app</a>.', ['@url' => 'https://developers.hubspot.com/docs/faq/how-do-i-create-an-app-in-hubspot']),
        $this->t('Get the Client ID and secret and enter the info here.'),
        $this->t('Click authorize and follow the prompts to connect your app.'),
        $this->t('View the <a href="@url" target="_blank">documentation here</a> for more info.', ['@url' => 'https://developers.hubspot.com/docs/overview']),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit authorize callback to Hubspot.
   */
  public function oauthAuthorizeSubmit(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('hubspot_api.settings');
    $client_id = $config->get('client_id');
    $client = new Client(['key' => $config->get('client_secret')]);
    $oauth = new OAuth2($client);
    $uri = $oauth->getAuthUrl(
      $client_id,
      Url::fromRoute('hubspot_api.oauth_redirect', [], ['absolute' => TRUE])->toString(),
      ['contacts']
    );

    $url = Url::fromuri($uri, ['external' => TRUE]);

    $form_state->setResponse(new TrustedRedirectResponse($url->toString()));
  }

  /**
   * Submit disconnect callback to Hubspot.
   */
  public function oauthDisconnectSubmit() {
    $this->configFactory->getEditable('hubspot_api.settings')->delete();
    $this->state->delete('hubspot_api_tokens');
  }

  /**
   * Submit disconnect callback to Hubspot.
   */
  public function oauthIsConnected() {
    $hubspot_api_tokens = $this->state->get('hubspot_api_tokens');
    $config = $this->configFactory->get('hubspot_api.settings');
    return (bool) $hubspot_api_tokens['access_token']
      && $hubspot_api_tokens['refresh_token']
      && $config->get('client_id')
      && $config->get('client_secret');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('hubspot_api.settings')
      ->set('access_key', $form_state->getValue('access_key'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->save();

    $this->state->set('hubspot_api_tokens', [
      'access_token' => $form_state->getValue('access_token'),
      'refresh_token' => $form_state->getValue('refresh_token'),
    ]);

    parent::submitForm($form, $form_state);
  }

}
